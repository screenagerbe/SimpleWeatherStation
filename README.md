# SimpleWeatherStation

This "weather station" can be used as a standalone device to monitor temperature, humidity, pressure and CO₂ level. Their values are displayed on the integrated 1.14" TFT display (240x135) as are date and time. Because it's build with the [ESPHome](https://epshome.io) technology you can easily link it to Home Assistant or any other DIY home automation system.
This example config file links the device to [Home Assistant (HA)](https://www.home-assistant.io/) where it's part of several automations.

It's a rather cheap and easy to build/configure indoor weather station and a nice little science project for those interested.

The TTGO tdisplay board can be replaced by any other ESP32 board with enough available GPIO's and memory (min. 1MB as this firmware takes about 975 KB in compiled format).

# Materials
1. ESPHome - I'm hosting ESPHome, Home Assistant, Traefik v2.4 and several other home automations tools in Docker on an Ubuntu Server 20.04.2 LTS 64bit running on a RPI4 8GB RAM with a 128GB SSD (via SATA over usb3).
2. [Bosch BME-280 3.3V sensor](https://www.otronic.nl/a-61520006/sensors/3-in-1-sensor-temperatuur-luchtvochtigheid-en-luchtdruk-3-3v-i2c-en-spi-bosch-bme280/): temperature, pressure and humidity
3. [Winsen MH-Z19B sensor](https://www.tinytronics.nl/shop/nl/sensoren/lucht/gas/winsen-mh-z19b-co2-sensor-met-kabel): CO₂ sensor (you could use an A- or C-model, but the B-model has header pins which makes it easier to connect this sensor to an ESP32 board)
4. [TTGO Tdisplay v1.1 ESP32 4MB with 1.14" TFT display](https://opencircuit.be/Product/TTGO-T-Display-V1.1-ESP32-1.14-inch-TFT)
5. 8 F-F jumper cables (or any other means to connect the sensors to the ESP32 board)
6. optional: [2000mAh LiPO battery](https://www.otronic.nl/a-61884317/batterijen/3-7v-2000mah-oplaadbare-lipo-lithium-polyemer-platte-batterij-103450/)
7. optional: LED(s)
8. optional: casing

You probably also need to do some soldering. The header pins are not preinstalled on the board and sensors.

# Connections

| Components | GPIO |
| ------ | ------ |
| BME-280 SDA | GPIO21 |
| BME-280 SCL | GPIO22 |
| MH-Z19B TX | GPIO25 |
| MH-Z19B RX | GPIO26 |

Make sure to connect the ground and power connections! The BME280 needs 3.3V, the MH-Z19B needs 5V.

# Inspiration:
- https://github.com/koenvervloesem/ESPHome-Air-Quality-Monitor
- https://esphome.io/cookbook/infostrip.html?highlight=mh%20z19b
- https://controlco2.space 

# License
This project is provided by Pieter Dewachter as open source software with the MIT license. See the LICENSE file for more information.

The C++/runtime codebase of the ESPHome project (file extensions .c, .cpp, .h, .hpp, .tcc, .ino) are published under the GPLv3 license. The Python codebase and all other parts of the ESPHome codebase are published under the MIT license. See the [ESPHome License](https://github.com/esphome/esphome/blob/dev/LICENSE) for more information.

# Disclaimer
The product links are linking to my local suppliers (no benefits whatsoever). 
No doubt you can find them cheaper on Ebay, Amazon or AliExpress.
